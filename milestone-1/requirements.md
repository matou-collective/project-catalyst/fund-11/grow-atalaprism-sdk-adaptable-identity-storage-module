# Adaptable Identity Storage Module Product Requirements

## Introduction

When AtalaPrism removed the Pluto implementation from `@atala/prism-wallet-sdk`,
it became clear that it would be very hard to use the SDK easily (for us, and
others) without some usable implementations. 

A conversation between the Ahau team and Javier
([@elribonazo](https://github.com/elribonazo)) lead to an emergent
collaboration. The result was a new module `@pluto-encrypted/database` which
made it orders of magnitude easier for communities to implement their own
storage. This module requires you to only implement a handful of simple methods,
and take care of the complexities like schema changes, migrations etc for you.

We wrote `@pluto-encrypted/leveldb` as a plugin for Ahau’s own specific needs,
and were able to demonstrate the stability of this in Node environments on
Desktop and Mobile.

We applied for a grant to deliver this work as we were exploring this space
together.

The success of this collaboration lead directly to changes seen in SDK version
5, where the `@pluto-encrypted/database` work has been integrated back into the
SDK. The collaboration in this case has proceeded faster than the grants
process. While a lot has been delivered, our work here is to take that work from
good prototypes to beautifully documented and accessible experience to make the
SDK truly accessible.

## Objective

The objective of this project is to develop a storage module that can be used as
part of `@atala/prism-wallet-sdk`. This will enable adopters to develop more
identity implementations, in turn supporting greater adoption of the SDK.

## Features

1. **Prism Wallet Agent Enhancement:**
    - Storage Module: Enable the Prism Wallet Agent to use a storage module to store DIDs, messages and credentials.
2. **Use Case Demonstration:**
    - Online Applications Integration: Update the `ssb-atala-prism` module to leverage enhanced wallet features
    - AtalaPrism PoC Update: Adapt the AtalaPrism Proof of Concept (PoC) to utilize updated agent APIs, showcasing practical application of the enhancements.
3. **Documentation:**
    - Documentation Updates: Revise project documentation to reflect feature changes and provide comprehensive guidance on usage and implementation.
4. **Testing and Quality Assurance:**
    - Testing Suite Update: Enhance the testing suite to encompass new features, ensuring thorough evaluation for reliability and security.
5. **Merge Pull Requests:**
    - Collaboration with Core Team: Coordinate with the Atala Prism core team to merge pull requests, integrating enhancements into the main codebase for broader accessibility.

## Functional Requirements

1. **Prism Wallet Agent Enhancement:**
    - Feature Addition: Storage Module
        - Develop the functionality for the Prism Wallet Agent to store dids, messages and credentials.
2. **Use Case Demonstration:**
    - Online Applications Integration:
        - Update the `ssb-atala-prism` module to leverage new changes - the wallet and cloud agents
        - Update AtalaPrism PoC to utilise updated agent API's
3. **Documentation:**
    - Make updates to the documentation to include the changes
4. **Testing and Quality Assurance:**
    - Update the testing suite to include feature changes to identify and address any issues.
5. **Merge Pull Requests:**
    - Communicate with Atala Prism core team to have contributions merged and made available to all prism developers

## Non Functional Requirements

1. **Performance:**
    - Ensure efficient performance of the storage module, minimizing latency in data retrieval and manipulation operations.
    - Optimize resource utilization to support scalability and accommodate increased user load.
2. **Security:**
    - Ensure robust security measures to protect stored data from unauthorized access or manipulation.
    - Employ encryption techniques to safeguard sensitive information stored within the wallet.
3. **Reliability:**
    - Ensure high reliability of the storage module, minimising the risk of data loss or corruption.
    - Implement mechanisms for data backup and recovery to mitigate potential failures.
4. **Compatibility:**
    - Ensure compatibility of the storage module with various operating systems and platforms supported by the Atala Prism wallet SDK.
    - Verify interoperability with existing wallet functionalities and third-party integrations.
5. **Usability:**
    - Design user-friendly interfaces for interacting with the storage module, facilitating ease of use and navigation.
    - Provide clear documentation and instructional materials to guide developers in implementing and utilising the new features effectively.

## User Stories

1. **As a new developer I can easily get started with the SDK**
    - The documentation clearly outlines how to use the storage module as apart of the SDK 
2. **As a developer with specific storage needs, I can easily implement a storage module for the SDK**
    - Clear documentation
    - Reference implementations
    - Test suite
3. **As a developer upgrading my SDK, I should not have to worry about the changing shape of data stored**
    - The abstract storage should manage any required migrations of data for each storage implementation
    - Implementations should only need to define basic methods.
4. **As a developer making a production release, I am well informed about the security considerations I need to make with regard to storage**
    - Identity and document different security considerations for the storage module

## Required Features

- Reference implementations for storage modules (e.g. LevelDB)
- Typescript types/ interfaces required for any storage implementation
- Test suite for testing new implementations
- Documentation / guide of how to write a storage implementation

### Risks

1. **Community Engagement Risk:**
    - Limited community participation may overlook important requirements.
    - Mitigation: Proactively engage and incentivize community involvement.
2. **Technical Complexity Risk:**
    - Unforeseen technical challenges may delay development.
    - Mitigation: Conduct thorough research, break tasks into manageable chunks, seek expert advice.
3. **Integration Risk:**
    - Integrating new functionalities may cause compatibility issues.
    - Mitigation: Conduct comprehensive compatibility testing, collaborate with maintainers.
4. **Timeline Risk:**
    - Delays in development or stakeholder approvals may extend the timeline.
    - Mitigation: Develop a realistic timeline, monitor progress, and adjust schedules.
5. **Resource Constraints Risk:**
    - Limited resources may impede project progress.
    - Mitigation: Conduct resource assessment, prioritise tasks, seek additional resources if needed.
6. **Quality Assurance Risk:**
    - Inadequate testing may result in unreliable software.
    - Mitigation: Implement rigorous testing methodologies, automate testing, conduct thorough regression testing.
7. **Documentation Risk:**
    - Incomplete documentation may hinder adoption and troubleshooting.
    - Mitigation: Prioritise comprehensive and updated documentation in multiple formats.
8. **Dependency Risk:**
    - Third-party dependencies may undergo changes or become deprecated.
    - Mitigation: Monitor dependencies, maintain version control, and stay informed about alternatives.

## Testing and Quality Assurance

- **Test Plan**
    1. **Unit Testing:**
        - Develop unit tests to verify the functionality for the Prism Wallet Agent to store dids, messages and credentials.
        - New Storage Modules: ensure compliance test is successful to validate a new storage module in @pluto-encrypted V5
    2. **Integration Testing:**
        - Perform integration tests to validate the functionality of the Prism Wallet Agent to store dids, messages and credentials.
        - Test scenarios involving multiple components working together, such as sending messages, listening for messages and storing dids, messages and credentials.
    3. **Security Testing:**
        - Perform security testing to identify and address potential vulnerabilities when sending messages, listening for messages and storing dids, messages and credentials.
        - Verify that authentication and authorisation mechanisms are robust and prevent unauthorised access.
        - Validate data encryption protocols to ensure the confidentiality of transmitted and stored data.
        - List questions to support informed security decisions around password for each module
    4. **Error Handling**
        - Conduct end-to-end tests to simulate real-world user scenarios, storing and retrieving DIDs and credentials
    5. **Performance Testing:**
        - Measure the response time and throughput of the system under different loads to ensure it meets performance requirements.
        - Stress test the system to determine its scalability and identify any bottlenecks or performance issues.
    6. **Reliability Testing:**
        - Test the availability and fault tolerance of the system by simulating various failure scenarios, such as server crashes or network outages.
        - Verify that the system can recover gracefully from failures without compromising user experience or data integrity.
        - Conduct disaster recovery tests to assess the effectiveness of recovery procedures in restoring the system and data in case of a catastrophic failure.
- **Quality Assurance Processes and Standards**
    - **Code Review:**
        - Implement a code review process to ensure adherence to coding standards and best practices.
        - Conduct peer reviews for all code changes to identify and address potential issues early in the development cycle.
    - **Documentation Standards:**
        - Follow documentation standards to ensure consistency and clarity in all project documentation.
        - Provide comprehensive documentation for developers and users, including API references, user guides, and release notes.
    - **Testing Standards:**
        - Adhere to industry-standard testing practices and methodologies such as black-box testing, white-box testing, and exploratory testing.
        - Maintain test coverage metrics to track the percentage of code covered by automated tests and ensure sufficient coverage across all components.

# Digital Identity Wallets: Research Report

## Introduction

The development of digital identity wallets using decentralized identifiers (DIDs) and verifiable credentials (VCs) has seen significant advancements. The AtalaPRISM TypeScript SDK facilitates rapid development of such wallets, but it requires robust and secure storage solutions. This report provides a summary of the research conducted as a part of the the Adaptable storage module project. The research included stakeholder interviews and workshops and case studies and outlines the necessary storage requirements and considerations for developing a storage module for the AtalaPRISM SDK.

## Case Studies

**1. MATTR Wallet**

MATTR Wallet, available on the Play Store, works with VDR (verifiable data registries) to store and securely share digital verifiable credentials. The wallet features a user-friendly onboarding experience with clear instructions and security measures. It also provides options for scanning QR codes, viewing and copying publicDIDs, observing wallet activity, and managing credentials. However, the recovery process of the wallet remains unclear.

**2. ProofSpace Platform & Wallet**

ProofSpace offers a comprehensive platform enabling the building, validation, and scaling of decentralized identity solutions. It adheres to high global technical and ethical standards in digital identity, as demonstrated by its ID2020 certification. The platform provides multi-level identity, decentralized governance, and privacy controls. However, the user experience could be overwhelming with a plethora of available services.

**3. RealMe - Transition to Verifiable Credentials**

RealMe is the New Zealand governments online identity service. The government identity products team has provided detailed insights into the implementation and transition towards Verifiable Credentials (VCs) within the context of New Zealand's digital identity ecosystem. They highlight the critical role of secure and flexible credential repositories, which can be deployed both locally and in the cloud. These repositories, facilitated by Verifiable Data Registries (VDRs), store and verify identifiers, public keys, and credential schemas. The team have published documents that emphasize the importance of robust security measures, compliance with regulatory standards, and the ability for holders to control their data via holder apps and provide technical information describing processes between holders applications and the differnet identity proofing and issuing identity services run by the New Zealand Government.

## Stakeholder Interviews

On the 6th of May our team interviewed Javier a senior member from the AtalaPrism development team and developer of the Pluto-Encrypted storage module. After introducing the objectives of the project and discussion outlined the functions of the storage and different implementations to support different use cases and requirements based on the wallet implementations. It also identified areas of continued development for the module to be more accessible to wallet implementations.

## **Storage Requirements**

1. **Security**
    - **Encryption**: Essential for protecting sensitive data. Implement robust encryption standards such as AES-256 to secure credentials, keys, and user information.
    - **Secure Storage**: Utilize platform-specific secure storage mechanisms (e.g., Keychain for iOS, Keystore for Android) to safeguard private keys and other critical data.
    - **Access Control**: Ensure strict access controls to prevent unauthorized access to stored data.
2. **Data Integrity**
    - **Tamper Detection**: Implement cryptographic hashes to detect and prevent data tampering.
    - **Backup and Recovery**: Provide encrypted backup options to ensure data can be recovered securely in case of loss or corruption.
3. **Storage Capacity**
    - **Efficient Data Management**: Optimize data storage using compression techniques to handle large amounts of credential data.
    - **Scalability**: Ensure the storage solution can scale to accommodate increasing data volumes.
4. **Performance**
    - **Speed and Responsiveness**: Optimize read/write operations for minimal latency, ensuring a smooth user experience.
    - **Cache Management**: Implement caching strategies to improve performance for frequently accessed data.
5. **Synchronization and Consistency**
    - **Offline Access**: Ensure the wallet functions offline with local data, synchronizing with remote sources when connectivity is restored.
    - **Data Consistency**: Maintain consistent local and remote data states to avoid discrepancies.
6. **User Experience**
    - **Seamless Operation**: Minimize latency and avoid frequent interruptions to enhance the user experience.
    - **User Control**: Allow users to manage their data, including export, import, backup, and deletion of credentials.
7. **Compatibility**
    - **Cross-Platform Support**: Ensure compatibility with various operating systems (e.g., Windows, macOS, Linux, iOS, Android).
    - **Interoperability**: Facilitate data sharing and use across different platforms and applications.
8. **Regulatory Compliance**
    - **Privacy and Data Protection**: Adhere to data protection regulations (e.g., GDPR, CCPA) to ensure user privacy.
    - **Audit Trails**: Maintain logs of data access and modifications for auditing purposes.

## **Important Considerations**

1. **Storage Mediums**
    - **Local Storage**: Secure local storage options like SQLite databases, secure file storage, or platform-specific solutions.
    - **Cloud Storage**: Ensure data is encrypted both at rest and in transit if using cloud storage, with secure APIs and enforced access controls.
2. **Data Model Design**
    - **Modular Structure**: Design a modular data model to allow for easy updates and expansions.
    - **Indexing**: Implement efficient indexing for fast data retrieval and queries.
3. **Key Management**
    - **Key Generation and Storage**: Securely generate and store cryptographic keys, utilizing hardware security modules (HSMs) or trusted execution environments (TEEs) when possible.
    - **Key Rotation and Revocation**: Implement policies for key rotation and mechanisms for key revocation to maintain security.
4. **Credential Management**
    - **Credential Issuance and Storage**: Ensure secure storage of issued credentials, including metadata like issuance and expiration dates.
    - **Verification and Presentation**: Provide secure mechanisms for credential verification and presentation.
5. **Developer Tools and Documentation**
    - **APIs and SDKs**: Provide clear APIs and comprehensive SDK documentation.
    - **Sample Code and Tutorials**: Offer sample code and tutorials to demonstrate best practices.

## Research recommendations

Based on the findings, we recommend the following for the project development team:

1. **Consider adaption of Pluto-Encrypted Module:** Since the launch of the project the Pluto-Encrypted storage module has now been implemented into the AtalaPrism SDK. The adaption of this module might be the best approach to meet project objectives.
2. **Consider ID2020 Certification:** Given its global recognition and trust, obtaining this certification could enhance the credibility of our platform.
3. **Prioritise User Experience:** The user interface should be intuitive and user-friendly. Overwhelming users with too many available services, as seen in ProofSpace, can be counterproductive.
4. **Ensure Privacy and Security:** Implement strong privacy controls, including selective disclosure and zero-knowledge predicates. The wallet should also provide secure onboarding options, such as biometrics or PIN.
5. **Clear Recovery Process:** It is vital to have a clear and secure recovery process to regain access to the wallet if needed.
6. **Credential Management:** With a potential increase in the number of credentials, the wallet should provide efficient credential management options.
7. **Educate Users:** Use technical terms to help educate users about digital identity and the wallet's operations.
8. **Provide Offline Credential Sharing:** This feature could be beneficial for users with limited internet access.

By considering these recommendations, we can develop a digital identity wallet that provides a seamless, secure, and user-friendly experience.

## Conclusion

Developing a storage module for the AtalaPRISM TypeScript SDK involves addressing comprehensive security, performance, and user experience requirements. Together the elements of the research along with the recommendations provide a clear gudie to ensure the creation of robust, scalable, and developer-friendly storage module for wallet applcations.

Prepared for Project Catalyst Project Grow AtalaPrism: Adaptable Storage Module by: Ben Tairea
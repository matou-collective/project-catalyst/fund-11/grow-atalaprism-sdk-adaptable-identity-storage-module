# Product Development Plan

## Stages

1. Plan, Analyze, Design
    - Product Requirements Document
    - Product Architecture Design
    - Product Development Plan ←THIS DOC
2. Build, Test
3. Deploy

---

## :calendar: (20th May - 10th June 2024)

### Update documentation
1. Update SDK README docs regarding Pluto:
    - [ ] Create - how to write a storage implementation
    - [ ] Update - with links out to community implementations
    - [ ] Create a checklist or series of questions to inform security considerations when choosing/ building an implementation
1. Update implementation docs:
    - [ ] Update `@pluto-encrypted/inmemory` : [README.md](https://github.com/atala-community-projects/pluto-encrypted/blob/master/packages/inmemory/README.md)
    - [ ] Update `@pluto-encryped/indexdb` : [README.md](https://github.com/atala-community-projects/pluto-encrypted/blob/master/packages/indexdb/README.md)
    - [ ] Update `@pluto-encrypted/leveldb` : [README.md](https://github.com/atala-community-projects/pluto-encrypted/blob/master/packages/leveldb/README.md)

1. Document the test-suite:
    - [ ] `@pluto-encrypted/test-suite` : [README.md](https://github.com/atala-community-projects/pluto-encrypted/blob/master/packages/test-suite/README.md)

1. Update Example code + docs:
    - [ ] `@pluto-encrypted/examples` : [all examples](https://github.com/atala-community-projects/pluto-encrypted/tree/master/examples)

1. Create a new home for `@pluto-encrypted` dev docs
    - [ ] explore using [docusaurus](https://docusaurus.io/) (or similar) to generate docs for each version release
    - [ ] create automation pipelines to make keeping these docs up to date easy


### Storage implementation guide

1. Create primer docs for those wanting to implement their own storage
    - [ ] Write the primer for those wanting to implement their own storage
    - [ ] Agree on a home for this guide with the AtalaPrism/ Identus team
    - [ ] Deploy primer guide for public use


### Storage security guide

1. Create a primer for developers which helps them make informed decisions which have the appropriate security properties for their use-case.
    - [ ] Write a primer for developers which helps them make informed decisions which have the appropriate security properties for their use-case.
    - [ ] Agree on a home for this guide with the AtalaPrism/ Identus team
    - [ ] Deploy primer guide for public use


### Deprecate modules

1. Remove deprecated modules from Pluto-Encrypted
    - [ ] Update Pluto Encrypted documentation to V5 and remove `@pluto-encrypted/database` module - has been folded into SDK
    - [ ] Update Pluto Encrypted documentation to V5 and remove `@pluto-encrypted/iagon` - was experimental, but not supported

---

## :calendar: (11th June - 20th June 2024)

### Milestone 2 Proof of Achievement (PoA) Reporting & Community Review

1. Create Slide Deck with links to Final Milestone evidence
1. Submit POA
    - [ ] Create a video showing highlights and evidence
    - [ ] Create the written report
    - [ ] Submit all PoA requirements in Project Catalyst
1. Community Review
    - [ ] Respond to PoA feedback and request for information
    - [ ] Receive confirmation of Approval

---

## :calendar: (20th June - 10th July 2024)

### Update `ssb-atala-prism`

1. Update pluto-encrypt is ssb-atalaprism repo from V4 to V5
    - [ ] Ensure all our integration tests work with the latest SDK (version 5)
    - [ ] Upgrade ssb-atalaprism module to use Atalaprism SDK version 5


### Update Ahau

1. Deploy to production new release with ssb-atalaprism upgrade to atalaprism V5
    - Desktop:
        - [ ] Develop and test new Ahau version to Linux, Windows and Mac
        - [ ] Update the CHANGELOG
        - [ ] Create a new version (tag) of Āhau
        - [ ] Create a new release
        - [ ] Publish the release
        - [ ] Share the link
    - Mobile
        - [ ] Develop and test new Ahau version to Android
        - [ ] Update the CHANGELOG
        - [ ] Create a new version (tag) of Āhau
        - [ ] Create a new release
        - [ ] Publish the release
        - [ ] Share the link

---

## :calendar: (11th July - 20th July 2024)

### Final Milestone Proof of Achievement (PoA) Reporting & Community Review
- Submit POA
    - [ ] Create Slide Deck with links to Final Milestone evidence
    - [ ] Create a video showing highlights and evidence
    - [ ] Create the written report
    - [ ] Submit all PoA requirements in Project Catalyst
- Community Review
    - [ ] Respond to PoA feedback and request for information
    - [ ] Receive confirmation of Approval
